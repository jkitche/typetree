export default {
	path: '/mnt/d/git/tstest/',
	name: 'tstest',
	type: 'folder',
	children: [
		{
			path: '/mnt/d/git/tstest/.babelrc',
			name: '.babelrc',
			type: 'file',
		},
		{
			path: '/mnt/d/git/tstest/.gitignore',
			name: '.gitignore',
			type: 'file',
		},
		{
			path: '/mnt/d/git/tstest/dist',
			name: 'dist',
			type: 'folder',
			children: [
				{
					path: '/mnt/d/git/tstest/dist/bundle.js',
					name: 'bundle.js',
					type: 'file',
				},
				{
					path: '/mnt/d/git/tstest/dist/bundle.js.map',
					name: 'bundle.js.map',
					type: 'file',
				},
				{
					path: '/mnt/d/git/tstest/dist/index.html',
					name: 'index.html',
					type: 'file',
				},
			],
		},
		{
			path: '/mnt/d/git/tstest/index.html',
			name: 'index.html',
			type: 'file',
		},
		{
			path: '/mnt/d/git/tstest/package.json',
			name: 'package.json',
			type: 'file',
		},
		{
			path: '/mnt/d/git/tstest/src',
			name: 'src',
			type: 'folder',
			children: [
				{
					path: '/mnt/d/git/tstest/src/components',
					name: 'components',
					type: 'folder',
					children: [
						{
							path: '/mnt/d/git/tstest/src/components/Tree',
							name: 'Tree',
							type: 'folder',
							children: [
								{
									path:
										'/mnt/d/git/tstest/src/components/Tree/TreeNode',
									name: 'TreeNode',
									type: 'folder',
									children: [
										{
											path:
												'/mnt/d/git/tstest/src/components/Tree/TreeNode/TreeNode.tsx',
											name: 'TreeNode.tsx',
											type: 'file',
										},
									],
								},
							],
						},
					],
				},
				{
					path: '/mnt/d/git/tstest/src/example.data.ts',
					name: 'example.data.ts',
					type: 'file',
				},
				{
					path: '/mnt/d/git/tstest/src/index.tsx',
					name: 'index.tsx',
					type: 'file',
				},
			],
		},
		{
			path: '/mnt/d/git/tstest/treegen.js',
			name: 'treegen.js',
			type: 'file',
		},
		{
			path: '/mnt/d/git/tstest/tsconfig.json',
			name: 'tsconfig.json',
			type: 'file',
		},
		{
			path: '/mnt/d/git/tstest/tslint.json',
			name: 'tslint.json',
			type: 'file',
		},
		{
			path: '/mnt/d/git/tstest/webpack.config.js',
			name: 'webpack.config.js',
			type: 'file',
		},
		{
			path: '/mnt/d/git/tstest/yarn-error.log',
			name: 'yarn-error.log',
			type: 'file',
		},
		{
			path: '/mnt/d/git/tstest/yarn.lock',
			name: 'yarn.lock',
			type: 'file',
		},
	],
};
