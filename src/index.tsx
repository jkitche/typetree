import * as React from "react";
import * as ReactDOM from "react-dom";
import TreeData from "./example.data";
import TreeNode, { ISelectionItem } from "./components/Tree/TreeNode/TreeNode";

const parsed = TreeData as ISelectionItem;
const element = <ul><TreeNode item={parsed} /></ul>;

ReactDOM.render(
	element,
	document.getElementById("example"),
);
