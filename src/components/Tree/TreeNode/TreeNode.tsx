import * as React from "react";
import * as TreeNodeCSS from "./tree_node.scss";

export interface ISelectionItem {
	path: string;
	name: string;
	type: 'folder' | 'file';
	children: Array<ISelectionItem>;
};

interface ITreeRootProps {
	item: ISelectionItem;
}

const getSelectionItemFromChildren = (children: Array<ISelectionItem>) => {
	return children.map((child: ISelectionItem) => {
		return (
			<TreeNode item={child} key={child.path} />
		)
	})
}

const getChildren = (item: ISelectionItem) => {
	let children = null;
	if (!('children' in item)) {
		return null;
	}
	return (
		<ul>
			{getSelectionItemFromChildren(item.children)}
		</ul>
	);
}

const TreeNode = (props: ITreeRootProps) => {
	const { item } = props;
	const children = getChildren(item);

	return (
		<li className={TreeNodeCSS.tree_node}>
			{item.name}
			{children}
		</li>
	);
}

export default TreeNode;
